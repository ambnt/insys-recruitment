import * as constants from "../constants";
import fetchJsonp from "fetch-jsonp";

export function fetchGallery(profile) {
  return async function (dispatch) {
    dispatch({
      type: constants.FETCH_GALLERY_REQUEST,
      profile
    });

    const gallery = await fetchJsonp(constants.API_URL([profile]), {
      method: "GET",
      jsonpCallbackFunction: "jsonFlickrFeed"
    });

    if (!gallery.ok) {
      alert("Error while downloading images");
      return dispatch({
        type: constants.FETCH_GALLERY_DONE,
        items: []
      });
    }

    const { items } = await gallery.json();
    return dispatch({
      type: constants.FETCH_GALLERY_DONE,
      items
    });
  };
}