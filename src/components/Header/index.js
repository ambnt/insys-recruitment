import React from "react";
import { Link, NavLink } from "react-router-dom";
import style from "./style.less";
import logo from "../../images/logo.jpg";
import Icon from "../Icon";

const links = [
  { to: "/", icon: "user", title: "Home page" },
  { to: "/gallery", icon: "image", title: "Gallery" }
];

const Header = () => {
  return (
    <header className={style.header}>
      <div className={style.header__background} />
      <h1 className={style.logo}>
        <Link to="/" tabIndex="1" accessKey="1">
          <img className={style.logo__image} alt="Marylin Monroe Profile" src={logo} />
        </Link>
      </h1>
      <nav className={style.nav}>
        {
          links.map(item => (
            <NavLink
              exact
              key={item.to}
              to={item.to}
              className={style.nav__icon}
              activeClassName={style["nav__icon--active"]}
            >
              <Icon name={item.icon} size={16} isAccessible={false}>{item.title}</Icon>
            </NavLink>
          ))
        }
      </nav>
    </header>
  );
};

export default Header;