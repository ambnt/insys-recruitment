/* eslint-disable import/no-named-as-default */
import React from "react";
import { Route, Switch } from "react-router-dom";
import PropTypes from "prop-types";
import { hot } from "react-hot-loader";
import { Helmet } from "react-helmet";
import style from "./style.less";

import Header from "../Header";
import HomePage from "../../containers/HomePage";
import NotFoundPage from "../../containers/NotFoundPage";
import GalleryPage from "../../containers/GalleryPage";

// This is a class-based component because the current
// version of hot reloading won't hot reload a stateless
// component at the top-level.

class App extends React.Component {
  render() {
    return (
      <div>
        <Helmet
            defaultTitle="Marylin Monroe"
            titleTemplate="%s - Marylin Monroe"
        />
        <Header />
        <main className={style.content}>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/gallery" component={GalleryPage} />
            <Route component={NotFoundPage} />
          </Switch>
        </main>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

export default hot(module)(App);