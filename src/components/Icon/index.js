import React from "react";
import PropTypes from "prop-types";
import style from "./style.less";
import styled from "styled-components";

function Icon({ className, name, children, isAccessible = true } ) {
  const classes = [className, style.icon, style[`icon--${name}`]].filter(Boolean).join(" ");
  const accessibleName = isAccessible && name.replace(/-/g, " ").replace(/\b\w/g, (l) => l.toUpperCase());
  return (
      <span className={classes}>
        {accessibleName}
        {children}
      </span>
  );
}

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  className: PropTypes.string,
  isAccessible: PropTypes.bool,
  children: PropTypes.any,
  size: PropTypes.number,
  color: PropTypes.string,
  align: PropTypes.string,
};

export default styled(Icon)`
  width: ${(props) => props.size || "16"}px;
  height: ${(props) => props.size || "16"}px;
  color: ${(props) => props.color || "inherit"};
  vertical-align: ${(props) => props.align || "baseline"};
  &:before {
    font-size: ${(props) => props.size || "16"}px;
  }
`;
