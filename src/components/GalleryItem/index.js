import React from "react";
import PropTypes from "prop-types";
import style from "./style.less";

const GalleryItem = ({ source, link, alt, isFetching = false }) => {
  const classes = [
    style.item,
    isFetching && style["item--fetching"]
  ].filter(Boolean).join(" ");

  return (
    <div className={style.item__container}>
      <div className={classes}>
        {
          !isFetching && (
            <a
              
              href={link}
              target="_blank"
              rel="noopener noreferrer"
            >
              <img
                className={style.image}
                src={source}
                alt={alt}
              />
            </a>
          )
        }
      </div>
    </div>
  );
};

GalleryItem.propTypes = {
  source: PropTypes.string,
  link: PropTypes.string,
  isFetching: PropTypes.bool,
  alt: PropTypes.string,
};

export default GalleryItem;