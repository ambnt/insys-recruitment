import React from "react";
import PropTypes from "prop-types";
import style from "./style.less";
import GalleryItem from "../GalleryItem";

const Gallery = ({ items, isFetching }) => {
  const galleryItems = items.slice(0, 9);
  galleryItems.length = 9; // for placeholders
  if (items.length < 9) {
    galleryItems.fill({}, items.length);
  }

  return (
    <div className={style.gallery}>
      {
        galleryItems.map(({ title, media: { m: source } = {}, link }, index) => (
          <GalleryItem
            key={link || index}
            title={title}
            source={source}
            link={link}
            isFetching={isFetching}
          />
        ))
      }
    </div>
  );
};

Gallery.propTypes = {
  items: PropTypes.array,
  isFetching: PropTypes.bool.isRequired
};

export default Gallery;