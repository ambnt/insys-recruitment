import React from "react";
import PropTypes from "prop-types";
import style from "./style.less";

const RichContent = ({ children }) => {
  return (
    <div className={style.content}>
      {children}
    </div>
  );
};

RichContent.propTypes = {
  children: PropTypes.element
};

export default RichContent;