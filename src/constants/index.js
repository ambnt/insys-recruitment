export const API_URL = (tags = []) =>
  `https://www.flickr.com/services/feeds/photos_public.gne?format=json&tags=${encodeURI(tags.map(tag => `"${tag}"`).join(","))}`;

export const FETCH_GALLERY_REQUEST = "FETCH_GALLERY_REQUEST";
export const FETCH_GALLERY_DONE = "FETCH_GALLERY_DONE";
