// Set up your root reducer here...
import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router"
import gallery from "./galleryReducer";

const rootReducer = history => combineReducers({
  router: connectRouter(history),
  gallery
});

export default rootReducer;