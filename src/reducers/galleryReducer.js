import * as constants from "../constants";
import objectAssign from "object-assign";
import initialState from "./initialState";

// IMPORTANT: Note that with Redux, state should NEVER be changed.
// State is considered immutable. Instead,
// create a copy of the state passed and set new values on the copy.
// Note that I"m using Object.assign to create a copy of current state
// and update values on the copy.
export default function galleryReducer(state = initialState.gallery, action) {
  switch (action.type) {
    case constants.FETCH_GALLERY_REQUEST:
      return objectAssign({}, state, { profile: action.profile, fetching: true });

    case constants.FETCH_GALLERY_DONE:
      return objectAssign({}, state, { items: action.items, fetching: false });

    default:
      return state;
  }
}