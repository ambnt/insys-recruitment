import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as actions from "../../actions/galleryActions";
import { Helmet } from "react-helmet";
import Gallery from "../../components/Gallery";

class GalleryPage extends React.Component {
  componentDidMount() {
    this.props.actions.fetchGallery("Marylin Monroe");
  }

  render () {
    const { fetching, items } = this.props.gallery;

    return (
      <article>
        <Helmet>
          <title>Gallery</title>
        </Helmet>
        <Gallery isFetching={fetching} items={items} />
      </article>
    );
  }
}

GalleryPage.propTypes = {
  actions: PropTypes.object.isRequired,
  gallery: PropTypes.object.isRequired,
  children: PropTypes.element
};

function mapStateToProps(state) {
  return {
    gallery: state.gallery
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GalleryPage);