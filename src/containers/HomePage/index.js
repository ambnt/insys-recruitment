import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import RichContent from "../../components/RichContent";

const HomePage = () => {
  return (
    <article>
      <Helmet>
        <title>Home</title>
      </Helmet>
      <RichContent>
        <header>
          <h1>Marylin Monroe</h1>
          <address>Poznan, PL</address>
        </header>
        <p>
          Litwo! Ojczyzno moja! Ty jesteś jak zdrowie. Ile cię trzeba było głucho w komety warkoczu słowem, ubiór galowy. szeptali niejedni, Że tym się uparta coraz głośniejsza kłótnia o jej wypadł suknia, a Pan świata wie, jak roratne świéce. Pierwsza z postawy lecz podmurowany. Świeciły się w cyfrę powiązany płotek połyskał się kupiecka ale razem ja powiem śmiało grzeczność prosił na tem, Że tym domu wiecznie będzie wojna u jednej strony swe trzymał pod lasem, i przeplatane różowymi wstęgi pośród nich brylant, niby zakryty od Nogajów! Prześladując w Litwie chodził tępy nie zdradzić swego roztargnienia: Prawda - niewola! Pamiętam, chociaż w podróży.
        </p>
        <blockquote>
          Etiam eu odio. Morbi accumsan, dolor eget enim. Nam vestibulum id, adipiscing quam et augue.
        </blockquote>
        <p>
          Wyższe założenie ideowe, a także skoordynowanie pracy obu urzędów umożliwia w przygotowaniu i unowocześniania postaw uczestników wobec zadań stanowionych przez organizację. W sumie inwestowanie w wypracowaniu systemu spełnia istotną rolę w określaniu kolejnych kroków w restrukturyzacji przedsiębiorstwa. Jak już zapewne zdążył zauważyć iż zmiana przestarzałego systemu szkolenia kadr rozszerza nam efekt dalszych kierunków rozwoju. I staje się iż zakres i rozwijanie struktur zmusza nas do wniosku, iż dalszy rozwój różnych form oddziaływania.
        </p>
      </RichContent>
    </article>
  );
};

HomePage.propTypes = {
  children: PropTypes.element
};

export default HomePage;