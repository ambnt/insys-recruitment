import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import RichContent from "../../components/RichContent";

const NotFoundPage = () => {
  return (
    <article>
      <Helmet>
        <title>Not Found</title>
      </Helmet>
      <RichContent>
        <h1>Error 404</h1>
        <span>Not found the page you&apos;ve requested.</span>
      </RichContent>
    </article>
  );
};

NotFoundPage.propTypes = {
  children: PropTypes.element
};

export default NotFoundPage;