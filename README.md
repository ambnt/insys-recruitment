# Zadanie testowe Insys

Zadanie rekrutacyjne dla firmy Insys na stanowisko front-end developera.

## Opis
Zadanie wykonano w technologii React w połączeniu z Reduxem, a sam kod zbudowano w oparciu o boilerplate [react-slingshot](https://github.com/coryhouse/react-slingshot). W trakcie tworzenia aplikacji, sporo rzeczy zmieniono lub uproszczono oraz zastosowano szereg innych modyfikacji w boilerplate:

- **zainstalowałem LESS** - uzywam styli LESS w systemie modułowym (z generowanymi, unikalnymi nazwami klas), dzięki czemu wyeliminowałem ryzyko kolizji z innym modułem o podobnej nazwie. Dodatkowo nazwy klas zostały oparte o konwencję BEM.
- **zmienne ze styli LESS wyciągnąłem do zbiorczego pliku variables.less** - przy okazji napisałem mixin .var(), który łączy funkcjonalność zmiennych LESS ze zmiennymi CSS. Zastosowanie zbiorczego pliku variables.less pozwala na znacznie łatwiejsze zmiany natury designerskiej, uspójnia powtarzające się elementy oraz moze być ogromnym ułatwieniem w przypadku chęci zaimplementowania elementów sub-brandingu lub kilku motywów kolorystycznych. Dzięki wykorzystaniu zmiennych CSS, wszystkie operacje na zmiennych w elemencie :root od razu wpływają na wygląd strony bez potrzeby implementowania takiej warstwy w JS, a zmienne LESS są w tym przypadku fallbackiem dla starszych przeglądarek.
- **napisałem komponent praktycznie niezniszczalnych ikon** :) - jako base wykorzystałem webfonta boxicons. Ikony w tej postaci mają jednak to do siebie, ze są tak naprawdę jakąś reprezentacją pewnych glifów z fontu, więc stanowią tak naprawdę zerową wartość dla czytników ekranów dla osób niepełnosprawnych. Taką ikonkę oprogramowanie ułatwienia dostępu przeczyta bowiem zgodnie z wartością glifu, więc będzie to coś pokroju "K" lub "性". Opracowany przeze mnie komponent ikon naprawia ten problem i zapewnia maksymalne accessibility, przekazując czytnikowi informację o ikonie w sposób niewidoczny dla normalnych uzytkowników. Przy okazji mój komponent oszczędzia trochę włosów na głowie rwanych przez developerów przy próbie opanowania skalowania ikon :)
- i kilka innych mniej lub bardziej waznych rzeczy

---------------------
# Get Started

1. **Initial Machine Setup**

    First time running the starter kit? Then complete the [Initial Machine Setup](#initial-machine-setup).

2. **Clone this repo**

3. **Run the setup script**

    `npm install`

4. **Run the app**

    `npm start -s`

    This will run the automated build process, start up a webserver, and open the application in your default browser. When doing development with this kit, this command will continue watching all your files.

## Initial Machine Setup

1. **Install [Node 8.0.0 or greater](https://nodejs.org)**

    Need to run multiple versions of Node? Use [nvm](https://github.com/creationix/nvm).

2. **Install [Git](https://git-scm.com/downloads)**.

3. **[Disable safe write in your editor](https://webpack.js.org/guides/development/#adjusting-your-text-editor)** to assure hot reloading works properly.

4. Complete the steps below for your operating system:

    ### macOS

    * Install [watchman](https://facebook.github.io/watchman/) via `brew install watchman` or fswatch via `brew install fswatch` to avoid [this issue](https://github.com/facebook/create-react-app/issues/871) which occurs if your macOS has no appropriate file watching service installed.

    ### Linux

    * Run this to [increase the limit](http://stackoverflow.com/questions/16748737/grunt-watch-error-waiting-fatal-error-watch-enospc) on the number of files Linux will watch. [Here's why](https://github.com/coryhouse/react-slingshot/issues/6).

        `echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p`.

    ### Windows
    
    * **Install [Python 2.7](https://www.python.org/downloads/)**. Some node modules may rely on node-gyp, which requires Python on Windows.
    * **Install C++ Compiler**. Browser-sync requires a C++ compiler on Windows.
    
      [Visual Studio Express](https://www.visualstudio.com/en-US/products/visual-studio-express-vs) comes bundled with a free C++ compiler.
      
      If you already have Visual Studio installed:
      Open Visual Studio and go to File -> New -> Project -> Visual C++ -> Install Visual C++ Tools for Windows Desktop.
      The C++ compiler is used to compile browser-sync (and perhaps other Node modules).